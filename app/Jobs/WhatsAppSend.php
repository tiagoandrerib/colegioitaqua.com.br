<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class WhatsAppSend implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $id, $name, $mobile;

    public function __construct($id, $name, $mobile)
    {
        $this->id = $id;
        $this->name = $name;
        $this->mobile = $mobile;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $url = 'https://eu8.chat-api.com/instance6654/message?token=2dcdbkun0p4v13px';

      
            $client = new \GuzzleHttp\Client(['verify' => false]);
            $response = $client->request('POST', $url, [
                'form_params' => [
                    'body' => "Olá $this->name!
Recebemos com sucesso sua inscrição no sorteio.

Seu número é *$this->id*. Acompanha o resultado diatiamente em nossa página no Facebook.

Aproveite para curtir nossa página.
www.facebook.com/marchaitaqua/",
                    'phone' => "55$this->mobile"
                ]
            ]);
        
    }
}