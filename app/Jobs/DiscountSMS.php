<?php

namespace App\Jobs;

use GuzzleHttp\Client;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class DiscountSMS implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    protected $subscriber;
    public function __construct($subscriber)
    {
        $this->subscriber = $subscriber;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $client = new Client();

        $name = explode(' ', $this->subscriber->name);

        $date = date('d/m', strtotime($this->subscriber->scheduled));

        $response = $client->request('POST', 'https://sms.tiagoandre.com.br/api/send', [
            'form_params' => [
                'reference' => $this->subscriber->id,
                'name' => $this->subscriber->name,
                'mobile' => str_replace(array( '(', ')','-',' ' ), '', $this->subscriber->mobile),
                'content' => "Colégio Itaquá: Olá ".$name[0]."! Sua solicitação de pré-agendamento foi realizada com sucesso! Em breve entraremos em contato.",
                'source' => "Colégio Itaquá"
            ]
        ]);

        $response = $response->getBody()->getContents();
    }
}
