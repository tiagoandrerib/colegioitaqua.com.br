<?php

namespace App\Jobs;

use GuzzleHttp\Client;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class DiscountWhats implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $subscriber;
    public function __construct($subscriber)
    {
        $this->subscriber = $subscriber;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
//dd($this->subscriber);

        //dd(\Storage::disk('local')->url($this->subscriber->image));

        //dd(\Storage::disk('local')->url($this->subscriber->id, $this->subscriber->image));

        $client = new Client();

        $name = explode(' ', $this->subscriber->name);

        $date = date('d/m', strtotime($this->subscriber->scheduled));

        $response = $client->request('POST', 'https://eu8.chat-api.com/instance6654/sendFile?token=2dcdbkun0p4v13px', [
            'form_params' => [
                'phone' => '55'.str_replace(array( '(', ')','-',' ' ), '', $this->subscriber->mobile),
                'body' => 'https://www.colegioitaqua.com.br/desconto/storage/'.$this->subscriber->image,
                'filename' => 'comprovante.png',
                'caption' => "Olá " . $name[0] . "! ! Sua solicitação de pré-agendamento foi realizada com sucesso! Em breve entraremos em contato.\n\nQualquer dúvida só me chamar \nwa.me/5511976534479"
            ]
        ]);

        $response = $response->getBody()->getContents();

    }


}
