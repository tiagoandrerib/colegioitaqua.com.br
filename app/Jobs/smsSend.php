<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class smsSend implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $id, $name, $mobile;


    public function __construct($id, $name, $mobile)
    {
        $this->id = $id;
        $this->name = $name;
        $this->mobile = $mobile;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, "https://api-rest.zenvia.com/services/send-sms");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_HEADER, FALSE);
    
    curl_setopt($ch, CURLOPT_POST, TRUE);
    
    curl_setopt($ch, CURLOPT_POSTFIELDS, "{
      \"sendSmsRequest\": {
        \"from\": \"Colégio Itaquá\",
        \"to\": \"55$this->mobile\",
        \"msg\": \"Olá $this->name recebemos com sucesso sua inscrição, e o número do protocolo é $this->id. Abraço e boa sorte.\",
        \"id\": \"$this->id\",
        \"flashSms\": false
      }
    }");
    
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
      "Content-Type: application/json",
      "Authorization: Basic bGljZXVicmFzaWwuY29ycHJlc3Q6Y0dSS1dFWkVpSg==",
      "Accept: application/json"
    ));
    
    $response = curl_exec($ch);
    curl_close($ch);
    
    //return var_dump($response);
    }   
}
