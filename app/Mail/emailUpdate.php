<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class emailUpdate extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    protected $subscriber;

    public function __construct($subscriber)
    {
        $this->subscriber = $subscriber;
    }
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        //dd($this->subscriber{'first_name'});

        return $this->subject('Inscrição atualizada com sucesso')
                    ->replyTo('contato@colegioitaqua.com.br')
                    ->view('mail.update')->with([
                        'first_name' => $this->subscriber{'first_name'},
                        'name' => $this->subscriber{'name'},
                        'id' => $this->subscriber{'id'},
                        'level' => $this->subscriber['level'],
                        'mobile' => $this->subscriber['mobile'],
                        'phone' => $this->subscriber['phone'],
                        'email' => $this->subscriber['email'],
                        'time' => $this->subscriber['time']
                    ]);
    }
}
