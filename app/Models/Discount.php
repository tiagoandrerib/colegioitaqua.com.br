<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Discount extends Model
{
    protected $fillable = [
        'name', 'mobile', 'level', 'phone', 'image', 'uuid', 'scheduled', 'is_visit'
    ];
}
