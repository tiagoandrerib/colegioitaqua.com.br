<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Subscriber;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use App\Jobs\WhatsAppSend;
use App\School;
use App\Jobs\smsSend;
use App\Jobs\mailSend;
use App\Jobs\mailUpdate;
use App\Jobs\confirmationSend;
use Illuminate\Support\Facades\Redirect;

class SubscriberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $schools = School::all();
        return view ('index', compact('schools'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('complete');  
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $mobile = str_replace(array( '(', ')','-',' ' ), '', $request->mobile);
        $request->request->add(['mobile' => $mobile]);
        
        /*$zipcode = str_replace(array( '(', ')','-' ), '', $request->zipcode);
        $request->request->add(['zipcode' => $zipcode]);*/
        
        $phone = str_replace(array( '(', ')','-',' ' ), '', $request->phone);
        $request->request->add(['phone' => $phone]);
        
        $first_name = explode(' ', $request->name, 2);
        $request->request->add(['first_name' => $first_name[0]]);

        /*$birth = Carbon::createFromFormat('d/m/Y', $request->birth)->format('Y-m-d');
        $request->request->add(['birth' => $birth]);*/

        $request->request->add(['font' => 'Colégio Itaquá Interesse']);

        $rules = [
            'name' => 'required',
            'mobile' => 'required|unique:subscribers|max:11|min:11',
            'email' => 'required|email|unique:subscribers',
            //'zipcode' => 'required',
            //'address' => 'required',
            //'number' => 'required',
            //'district' => 'required',
            //'city' => 'required',
            //'state' =>'required',
            //'school' =>'required',
            //'period' =>'required',
            //'time' =>'required',
            //'student' => 'required',
            //'responsible' => 'required',
            'level' => 'required'

        ];

        $messages = [
            'name.required' => 'O campo nome é obrigatório.',
            'mobile.required' => 'O campo número do celular é obrigatório.',
            'mobile.min' => 'Digite o número do celular (WhatsApp) com DDD.',
            'mobile.max' => 'Digite o número do celular (WhatsApp) com DDD.',
            'mobile.unique' => 'Número de celular já cadastrado.',
            'phone.unique' => 'Número de telefone fixo já cadastrado.',
            'phone.required' => 'O campo telefone fixo é obrigatório.',
            'phone.min' => 'Digite o número do telefone fixo com DDD.',
            'phone.max' => 'Digite o número do telefone fixo com DDD.',
            'email.required' => 'O campo de e-mail é obrigatório.',
            'email.unique' => 'Endereço de e-mail já está cadastrado.',
            'document.required' => 'O campo de RG é obrigatório.',
            'document.unique' => 'Número de documento já cadastrado.',
            'birth.required' => 'O campo data de nascimento é obrigatório.',
            'zipcode.required' => 'O campo CEP é obrigatório.',
            'address.required' => 'O campo endereço é obrigatório.',
            'number.required' => 'O campo número é obrigatório.',
            'district.required' => 'O campo bairro é obrigatório.',
            'city.required' => 'O campo cidade é obrigatório.',
            'state.required' =>'O campo estado é obrigatório.',
        ];

        $subscriber = Validator::make($request->all(), $rules, $messages);

        if ($subscriber->fails()) {

            //$birth = $request->birth = Carbon::createFromFormat('Y-m-d', $request->birth)->format('d/m/Y');
            //$request->request->add(['birth' => $birth]);
            return redirect('/')
                        ->withErrors($subscriber)
                        ->withInput();
        }

        //dd($request->all());
        $subscriber = new Subscriber($request->all());
        $subscriber->save();

        //WhatsAppSend::dispatch($subscriber->id, $subscriber->first_name, $subscriber->mobile);

        $email = $request->email;

        //smsSend::dispatch($subscriber->id, $subscriber->first_name, $subscriber->mobile);
        //mailSend::dispatch($email, $subscriber);

        return Redirect::to('/')->with('message', 'Obrigado! Sua solicitação foi registrada com sucesso no sistema.');  
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //$schools = School::all();
        $subscribers = Subscriber::where('id', $id)->first();
        $email = $subscribers->email;
        confirmationSend::dispatch($email, $subscribers);

        //dd($subscribers->id);

        $name = $subscribers->first_name;
        $id = $subscribers->id;
        $time = $subscribers->time;

        Subscriber::where('id', $subscribers->id)->update( array('confirmation'=>'SIM') );
        return view ('confirmation', compact('id', 'name', 'time'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $schools = School::all();
        $subscribers = Subscriber::where('id', $id)->get();

        return view ('update', compact('subscribers', 'schools'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    
    {
        $mobile = str_replace(array( '(', ')','-',' ' ), '', $request->mobile);
        $request->request->add(['mobile' => $mobile]);
                
        $phone = str_replace(array( '(', ')','-',' ' ), '', $request->phone);
        $request->request->add(['phone' => $phone]);
        
        $first_name = explode(' ', $request->name, 2);
        $request->request->add(['first_name' => $first_name[0]]);

        $birth = Carbon::createFromFormat('d/m/Y', $request->birth)->format('Y-m-d');
        $request->request->add(['birth' => $birth]);

        $request->request->add(['font' => 'Colégio Itaquá Bolsa 2018']);

        $first_name = explode(' ', $request->name, 2);
        $request->request->add(['first_name' => $first_name[0]]);

        unset($request['_token']);
        $subscribers =  Subscriber::where('id', $id)->update($request->all());

        $id = $id;

        $email = $request->email;

        $subscriber = $request->all();
        //dd( $subscriber);

        mailUpdate::dispatch($email, $subscriber);

        return view ('atualizado', compact('id'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}