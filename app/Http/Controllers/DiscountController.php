<?php

namespace App\Http\Controllers;

use App\Jobs\DiscountSMS;
use App\Jobs\DiscountWhats;
use App\Models\Discount;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;
use Spipu\Html2Pdf\Html2Pdf;
use SimpleSoftwareIO\QrCode\BaconQrCodeGenerator;
use Illuminate\Support\Str;
use Ramsey\Uuid\Uuid;

class DiscountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view ('site.discount.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {



        $today = Carbon::now();

        if($today->dayOfWeek == Carbon::SATURDAY) {
            $date = new Carbon('next week');
            $date = $today->addDay(2)->format('d/m/Y');
            //dd(['today' => $today->format('d/m'), 'date' => $date = $today->addDay(2)->format('d/m')]);
        }
        else {
            $date = new Carbon('next week');
            $date = $today->addDay(1)->format('d/m/Y');
            //dd(['today2' => $today->format('d/m'), 'date' => $date = $today->addDay(1)->format('d/m')]);
        }


        //dd($request->all());
        $rules = [
            'name' => 'required',
            'mobile' => 'required|unique:discounts',
            'level' => 'required'


        ];

        $messages = [
            'name.required' => 'Insira o nome completo.',
            'mobile.required' => 'Insira o número do WhatsApp',
            'mobile.unique' => 'WhatsApp já cadastrado',
            'level.required' => 'Selecione a série do aluno'

        ];

        //dd($request->all());

        $subscriber = \Validator::make($request->all(), $rules, $messages);

        if ($subscriber->fails()) {
            return redirect()->back()
                ->withErrors($subscriber)
                ->withInput();
        }




        $request->request->add(['uuid' => Uuid::uuid4()]);
        $request->request->add(['scheduled' => date('Y-m-d', strtotime(str_replace('/', '-', $date)))]);

        //dd(date('d/m', strtotime($request->scheduled)));

        $subscriber = Discount::create($request->all());



        DiscountSMS::dispatch($subscriber);


        return view('site.discount.thanks', compact('subscriber', 'date'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function test(Request $request)
    {

        $qrcode = new BaconQrCodeGenerator;
         $qrcode->format('png')->size(500);

        $image = $qrcode->generate('Make a qrcode without Laravel!');

        Storage::disk('local')->put(  $image );

        $date = Carbon::now()->addDay(1)->format('d/m');

       // dd($date);

        $today = Carbon::now();

        if($today->dayOfWeek == Carbon::SATURDAY) {
            $date = new Carbon('next week');
            $date = $today->addDay(2)->format('d/m');
            //dd(['today' => $today->format('d/m'), 'date' => $date = $today->addDay(2)->format('d/m')]);
        }
        else {
            $date = new Carbon('next week');
            $date = $today->addDay(1)->format('d/m');
            //dd(['today2' => $today->format('d/m'), 'date' => $date = $today->addDay(1)->format('d/m')]);
        }

        return view('site.discount.thanks', compact('date'));
    }


    public function test2(Request $request)
    {

        $image = $request->image;  // your base64 encoded
        $image = str_replace('data:image/png;base64,', '', $image);
        $image = str_replace(' ', '+', $image);
        $imageName = 'comprovante-'.str_random(10).'.'.'png';
        //$image = \File::put(storage_path(). '/app/images/' . $imageName, base64_decode($image));


        $image = \Storage::disk('public')->put($imageName, base64_decode($image));


        $update = Discount::where('id', $request->id)->first();

        if(empty($update->image)){
            $update = Discount::where('id', $request->id)->update([
                'image' => $imageName,
            ]);
        }

        $user = Discount::where('id', $request->id)->first();

        DiscountWhats::dispatch($user);



        return $imageName;
    }

    public function download(Request $request)
    {

       $image = Discount::where('id', $request->id)->first();


        return \Storage::disk('public')->download($image->image);

    }


    public function visit(Request $request)
    {

        //dd($request->all());

        $update = Discount::where('uuid', $request->uuid)->update([
            'is_visit' => 1
        ]);


        return $update;

    }
}
