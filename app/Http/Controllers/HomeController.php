<?php

namespace App\Http\Controllers;

use App\Exports\RegistrationFilter;
use App\Models\Registration;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Maatwebsite\Excel\Concerns\WithHeadings;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $itaqua = Registration::where('place', 1)->get();
        $suzano = Registration::where('place', 3)->get();
        $ferraz = Registration::where('place', 4)->get();

        $itaquaFilter = $itaqua->where('is_student', 0)->count();
        $suzanoFilter = $suzano->where('is_student', 0)->count();
        $ferrazFilter = $ferraz->where('is_student', 0)->count();

        return view('home', compact ('itaqua', 'suzano', 'ferraz', 'itaquaFilter', 'suzanoFilter', 'ferrazFilter'));
    }

    public function export(Request $request)
    {

        //dd($request->all());
       // return Excel::download(new RegistrationFilter(), 'users.xlsx');

        $date = Carbon::now()->subDays(1)->format('Y-m-d');


        $id = $request->id;
        $place = $request->place;

        //dd($date);

        return (new RegistrationFilter($date, $id, $place))->download('liceu-brasil-'.$date.'.xlsx');
    }
}
