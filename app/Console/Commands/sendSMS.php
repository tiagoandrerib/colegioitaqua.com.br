<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;

class sendSMS extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:SMS';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Enviar SMS';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $subscribers = DB::table('subscribers')
                            ->where('level', '!=', 'N1 da Educação Infantil')
                            ->where('level', '!=', 'N2 da Educação Infantil')
                            ->where('level', '!=', 'N3 da Educação Infantil')
                            ->where('level', '!=', '1ª ano do Ensino Fundamental')
                            ->where('level', '!=', '2ª ano do Ensino Fundamental')
                            ->where('level', '!=', '3ª ano do Ensino Médio')
                           
                            ->get();

                            //dd($subscribers);

                            foreach($subscribers as $subscriber) {   
                               
                                
                                $url = 'https://eu8.chat-api.com/instance6654/sendFile?token=2dcdbkun0p4v13px';
                $client = new \GuzzleHttp\Client(['verify' => false]);
                $response = $client->request('POST', $url, [
                    'form_params' =>  [
                        'filename'     => "concurso.jpg",
                        'body' => "https://tiagoandre.com.br/whatsapp/storage/uploads/vcw10XIuOdL8bCPN933bJDlG57JXxsu8gcpTqVsD.jpeg",
                        'phone' => "55$subscriber->mobile",
                    ],
                ]);
                //sleep(1);
                
                                


                            }
                            
                            
    }
}
