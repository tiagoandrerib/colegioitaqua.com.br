<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use App\Notifications\mailAlert;
use App\Jobs\mailUpdate;

use App\Mail\emailSend;
use Illuminate\Support\Facades\Mail;


class SendMails extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:mailAlert  ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $subscribers = DB::table('subscribers')
                            ->where('level', '!=', 'N1 da Educação Infantil')
                            ->where('level', '!=', 'N2 da Educação Infantil')
                            ->where('level', '!=', 'N3 da Educação Infantil')
                            ->where('level', '!=', '1ª ano do Ensino Fundamental')
                            ->where('level', '!=', '2ª ano do Ensino Fundamental')
                            ->where('level', '!=', '3ª ano do Ensino Médio')
                            ->get();

                            //dd($subscribers);

                            foreach($subscribers as $subscriber) {   
                                $email = $subscriber->email;
                                //mailUpdate::dispatch($email);
                                Mail::to($email)->send(new emailSend($subscriber));
                                echo $subscriber->id;
                            }
                            
                            
    }
}
