<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use DB;
use Illuminate\Notifications\Notifiable;

class Subscriber extends Model
{
    use Notifiable;

    protected $fillable = [
        'id', 'name', 'birth', 'mobile', 'responsible', 'phone', 'email', 'zipcode', 'time', 'number', 'address2', 'address', 'city', 'state', 'first_name', 'district', 'authorize', 'font', 'student', 'level', 'period', 'school'
    ];

    public function count()
    {
        return $count = Subscriber::all()->count();
    }

    public function groupsBy()
    {
        return $groupsBy = DB::table('subscribers')
                            ->select('level', DB::raw('count(*) as total'))
                            ->groupBy('level') 
                            ->orderBy('total', 'DESC')   
                            ->pluck('total','level')
                            ->all();

    }

    public function times()
    {
        return $time = DB::table('subscribers')
                        ->select('time', DB::raw('count(*) as total'))
                        ->groupBy('time')
                        ->orderBy('total', 'DESC')
                        ->pluck('total', 'time')
                        ->all();
    }

}