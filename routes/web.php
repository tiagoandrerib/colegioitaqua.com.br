<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', 'SubscriberController@index')->name('index');


Route::post('/obrigado', 'SubscriberController@store')->name('store');

Route::get('/complete', 'SubscriberController@create')->name('complete');

Auth::routes();
Route::get('/admin', 'HomeController@index')->name('admin.index');
Route::get('/admin/cadastros', 'HomeController@subscriber')->name('admin.subscribers');

Route::get('/cadastro/{id}', 'SubscriberController@edit')->name('cadastro');
Route::post('/update/{id}', 'SubscriberController@update')->name('update');

Route::get('/confirmar/{id}', 'SubscriberController@show')->name('confirmar');

Route::get('admin/students', 'StudentController@students');
Route::resource('admin/notas-faltas', 'StudentController');


Route::get('/email', function() {
    return view ('mail.confirmation');
});



Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('home/export/', 'HomeController@export')->name('export');

Route::get('/', 'DiscountController@index')->name('discount.index');
Route::post('agendamento', 'DiscountController@store')->name('discount.store');




Route::get('test', 'DiscountController@test')->name('discount.test');
Route::post('{id}/update', 'DiscountController@test2')->name('discount.test2');
Route::get('download', 'DiscountController@download')->name('download');
