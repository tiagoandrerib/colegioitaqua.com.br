<!DOCTYPE html>
<html lang="pt-BR">

<head>
    <title>Concurso de Bolsa Colégio Itaquá 2018</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ URL::asset('css/main.css') }}">
    <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
    {!! AnalyticsTracking::render() !!}
</head>

<body>
    <header class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                <img src="{{URL::asset('/img/logo.png')}}" class="img-fluid" />
            </div>
        </div>
    </header>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-10 col-lg-10 col-md-12 col-sm-12 col-12">
                <h3>Atualizar inscrição</h3>
                <p>Digite atentamente seus dados abaixo, pois será através deles que entraremos em contato com você!</p>
                @foreach ($subscribers as $subscriber)
                <form method="POST" action="{{ url("/update/$subscriber->id") }}">
                    {{csrf_field()}} @if ($errors->any())
                    <div class="alert alert-danger">
                        Foram encontrados alguns erros no preenchimento
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif

                    <input name="id" hidden value="{{$subscriber->id}}">
                    <div class=" form-row">
                        <div class=" form-group col-xl-8 col-lg-8 col-md-8 col-sm-12 col-12">
                            <label class="col-form-label-lg" for="name">Nome Completo</label>
                            <input required type="text" class=" form-control form-control-lg" name="name" value="{{ $subscriber->name }}" id="name" placeholder="Digite seu nome completo">
                        </div>
                        <div class=" form-group col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                            <label class="col-form-label-lg" for="birth">Data de nascimento</label>
                            <input required type="text" class=" form-control form-control-lg" name="birth" value="{{ \Carbon\Carbon::createFromFormat('Y-m-d', $subscriber->birth)->format('d/m/Y') }}" id="birth" data-mask="00/00/0000" placeholder="dd/mm/aaaa">
                        </div>
                        <div class=" form-group col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <label class="col-form-label-lg" for="responsible">Responsável</label>
                            <input required type="text" class=" form-control form-control-lg" name="responsible" value="{{ $subscriber->responsible }}" id="responsible" placeholder="Digite o nome completo do responsável">
                        </div>
                        <div class=" form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                            <label class="col-form-label-lg" for="mobile">WhatsApp (Celular)</label>
                            <input required type="text" class=" form-control form-control-lg" name="mobile" value="{{ $subscriber->mobile }}" id="mobile" data-mask="(00) 0 0000-0000" placeholder="(11) 9xxxx-xxxx">
                        </div>
                        <div class=" form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                            <label class="col-form-label-lg" for="phone">Telefone Fixo</label>
                            <input type="text" class=" form-control form-control-lg" name="phone" value="{{ $subscriber->phone }}" id="phone" data-mask="(00) 0000-0000" placeholder="(11) xxxx-xxxx">
                        </div>
                        <div class=" form-group col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
                            <label class="col-form-label-lg" for="email">E-mail</label>
                            <input required type="email" class=" form-control form-control-lg" name="email" id="email" value="{{ $subscriber->email }}" placeholder="seu@email.com">
                        </div>
                        <!--
                        <div class=" form-group col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                            <label class="col-form-label-lg" for="zipcode">CEP</label>
                            <input required type="text" class=" form-control form-control-lg" name="zipcode" id="zipcode" value="{{ $subscriber->zipcode }}" data-mask="00000-000" placeholder="00000-000">
                        </div>
                        <div class=" form-group col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                            <label class="col-form-label-lg" for="address">Endereço</label>
                            <input required type="text" class=" form-control form-control-lg" readonly="readonly" name="address" value="{{ $subscriber->address}}" placeholder="Nome da rua, estrada, avenida...">
                        </div>
                        <div class=" form-group col-xl-3 col-lg-3 col-md-3 col-sm-4 col-4">
                            <label class="col-form-label-lg" for="number">Número</label>
                            <input required type="text" class=" form-control form-control-lg" name="number" id="number" value="{{ $subscriber->number }}" placeholder="Número">
                        </div>
                        <div class=" form-group col-xl-3 col-lg-3 col-md-3 col-sm-8 col-8">
                            <label class="col-form-label-lg" for="address2">Complemento</label>
                            <input type="text" class=" form-control form-control-lg" name="address2" id="address2" value="{{ $subscriber->ddress2 }}" placeholder="Complemento">
                        </div>
                        <div class=" form-group col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                            <label class="col-form-label-lg" for="district">Bairro</label>
                            <input required type="text" class=" form-control form-control-lg" readonly="readonly" value="{{ $subscriber->district }}" name="district" id="district" placeholder="Bairro">
                        </div>
                        <div class=" form-group col-xl-3 col-lg-3 col-md-3 col-sm-10 col-10">
                            <label class="col-form-label-lg" for="city">Cidade</label>
                            <input required type="text" class=" form-control form-control-lg" readonly="readonly" name="city" value="{{ $subscriber->city }}" id="city" placeholder="Cidade">
                        </div>
                        <div class=" form-group col-xl-3 col-lg-3 col-md-3 col-sm-2 col-2">
                            <label class="col-form-label-lg" for="state">Estado</label>
                            <input required type="text" class=" form-control form-control-lg" readonly="readonly" name="state" value="{{ $subscriber->state }}" id="state" placeholder="UF">
                        </div>-->
                        <div class=" form-group col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                            <label class="col-form-label-lg" for="school">Escola</label>
                            <input required list="school" name="school" value="{{$subscriber->school}}" class=" form-control form-control-lg" placeholder="Digite o nome da escola">
                            <datalist id="school">
                                @foreach($schools as $school)
                                <option value="{{$school->name}}" > @endforeach
                            </datalist>
                            <small id="emailHelp" class="form-text text-muted">Nome da escola que estuda atualmente</small>
                        </div>
                        <div class=" form-group col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                            <label class="col-form-label-lg" for="level">Ano</label>
                            <select class="form-control form-control-lg" name="level" id="level" required>
                                <option disabled selected hidden>Selecione</option>
                                <!--<option {{ $subscriber->level =="N1 da Educação Infantil" ? 'selected' : '' }} value="N1 da Educação Infantil">N1 da Educação Infantil </option>
                                <option {{ $subscriber->level =="N2 da Educação Infantil" ? 'selected' : '' }} value="N2 da Educação Infantil">N2 da Educação Infantil </option>
                                <option {{ $subscriber->level=="N3 da Educação Infantil" ? 'selected' : '' }} value="N3 da Educação Infantil">N3 da Educação Infantil </option>
                                <option {{ $subscriber->level=="1ª ano do Ensino Fundamental" ? 'selected' : '' }} value="1ª ano do Ensino Fundamental">1ª ano do Ensino Fundamental</option>
                                <option {{ $subscriber->level=="2ª ano do Ensino Fundamental" ? 'selected' : '' }} value="2ª ano do Ensino Fundamental">2ª ano do Ensino Fundamental</option>
                                --><option {{ $subscriber->level=="3ª ano do Ensino Fundamental" ? 'selected' : '' }} value="3ª ano do Ensino Fundamental">3ª ano do Ensino Fundamental</option>
                                <option {{ $subscriber->level=="4ª ano do Ensino Fundamental" ? 'selected' : '' }} value="4ª ano do Ensino Fundamental">4ª ano do Ensino Fundamental</option>
                                <option {{ $subscriber->level=="5ª ano do Ensino Fundamental" ? 'selected' : '' }} value="5ª ano do Ensino Fundamental">5ª ano do Ensino Fundamental</option>
                                <option {{ $subscriber->level=="6ª ano do Ensino Fundamental" ? 'selected' : '' }} value="6ª ano do Ensino Fundamental">6ª ano do Ensino Fundamental</option>
                                <option {{ $subscriber->level=="7ª ano do Ensino Fundamental" ? 'selected' : '' }} value="7ª ano do Ensino Fundamental">7ª ano do Ensino Fundamental</option>
                                <option {{ $subscriber->level=="8ª ano do Ensino Fundamental" ? 'selected' : '' }} value="8ª ano do Ensino Fundamental">8ª ano do Ensino Fundamental</option>
                                <option {{ $subscriber->level=="9ª ano do Ensino Fundamental" ? 'selected' : '' }} value="9ª ano do Ensino Fundamental">9ª ano do Ensino Fundamental</option>
                                <option {{ $subscriber->level=="1ª ano do Ensino Médio" ? 'selected' : '' }} value="1ª ano do Ensino Médio">1ª ano do Ensino Médio</option>
                                <option {{ $subscriber->level=="2ª ano do Ensino Médio" ? 'selected' : '' }} value="2ª ano do Ensino Médio">2ª ano do Ensino Médio</option>
                                <!--<option {{ $subscriber->level=="3ª ano do Ensino Médio" ? 'selected' : '' }} value="3ª ano do Ensino Médio">3ª ano do Ensino Médio</option>-->
                            </select>
                            <small id="emailHelp" class="form-text text-muted">Série atual do aluno</small>
                        </div>
                        <div class="form-group col-xl-2 col-lg-2 col-md-2 col-sm-12 col-12">
                            <label class="col-form-label-lg" for="period">Perídodo</label>
                            <select class="form-control form-control-lg" name="period" id="period">
                                <option value="" disabled selected hidden>Selecione</option>
                                <option value="Manhã" {{ $subscriber->period =="Manhã" ? 'selected' : '' }}>Manhã</option>
                                <option value="Tarde" {{ $subscriber->period =="Tarde" ? 'selected' : '' }}>Tarde</option>
                                <option value="Noite" {{ $subscriber->period =="Noite" ? 'selected' : '' }}>Noite</option>
                                <option value="Integral" {{ $subscriber->period =="Integral" ? 'selected' : '' }}>Integral</option>
                            </select>
                            <small id="emailHelp" class="form-text text-muted">Período atual do aluno</small>
                        </div>
                        <hr>
                        <div class=" form-group col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <label class="col-form-label-lg" for="time">Horário da prova</label>
                            <select class="form-control form-control-lg" name="time" id="time" required>
                                <option value="" disabled selected hidden>Selecione</option>
                                <option {{ $subscriber->time =="09:00" ? 'selected' : '' }} value="09:00">01 de setembro às 09:00</option>
                                <option {{ $subscriber->time =="14:00" ? 'selected' : '' }} value="14:00">01 de setembro às 14:00</option>
                            </select>
                        </div>
                        <div class=" form-group col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <label class="col-form-label-lg" for="student">Você é aluno do Colégio Itaquá?</label>
                            <select class="form-control form-control-lg" name="student" id="student">
                                <option value="" disabled selected hidden>Selecione</option>
                                <option {{ $subscriber->student =="Sim" ? 'selected' : '' }} value="Sim">Sim, estou estudando no Colégio Itaquá</option>
                                <option {{ $subscriber->student =="Não" ? 'selected' : '' }} value="Não">Não, não estudo no Colégio Itaquá</option>
                            </select>
                        </div>
                        <div class=" form-group">
                            <div class="form-check">
                                <input required type="checkbox" class="form-check-input required" name="authorize" id="authorize" checked>
                                <label class="col-form" class=" form-check-label" for="authorize">Desejo receber as novidades e promoções do Colégio Itaquá</label>
                            </div>
                        </div>
                    </div>
                    @endforeach
                    <button type="submit" class="btn btn-primary btn-lg float-right">SALVAR ALTERAÇÕES</button>
                </form>
            </div>
        </div>
    </div>
    <footer class="container-fluid ">
        <div class="col-12">
            <div class="row justify-content-center">
                <img src="{{URL::asset('/img/logo-colegio-itaqua.png')}}" class="img-footer" />
            </div>
            <div class="text-footer">
                <h4>4647-0763 | 2829-4621<br/>
                  <small>Av. ítalo Adami, 1422,  Vila Zeferina
                  Itaquaquecetuba -SP</small>
               </h4>
            <a href="{{URL::asset('regulamento-concurso-bolsa-2019.pdf')}}"  target="_blank">Consulte o regulamento</a>
            </div>
        </div>
    </footer>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <script type="text/javascript" src="{{URL::asset('js/jquery.mask.js')}}"></script>
    <script type="text/javascript" src="{{URL::asset('js/mask.js')}}"></script>
    <script type="text/javascript" src="{{URL::asset('js/zipcode.js')}}"></script>
</body>

</html>