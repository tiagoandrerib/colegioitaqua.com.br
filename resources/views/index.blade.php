<!DOCTYPE html>
<html lang="pt-BR">

<head>
    <title>Concurso de Bolsa Colégio Itaquá 2018</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ URL::asset('css/main.css') }}">
    <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
    {!! AnalyticsTracking::render() !!}
</head>

<body>
  <!--
<header>
 Fixed navbar
  <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
    <a class="navbar-brand" href="#">Fixed navbar</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarCollapse">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item active">
          <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">Link</a>
        </li>
        <li class="nav-item">
          <a class="nav-link disabled" href="#">Disabled</a>
        </li>
      </ul>
      <form class="form-inline mt-2 mt-md-0">
        <input class="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search">
        <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
      </form>
    </div>
  </nav>
</header> -->

<!-- Begin page content -->
<main role="main" class="container">
<div class="container" style="margin-top: 50px; margin-bottom: 50px;">
        <div class="row justify-content-center">
            <div class="col-xl-10 col-lg-10 col-md-12 col-sm-12 col-12">
                <h3>Formulário de Interesse</h3>
                <p>Preencha o formulário abaixo para que possamos entrar em contato e fornecer mais informações.</p>
                <form method="POST" action="{{ route('store') }}">
                    {{csrf_field()}} @if ($errors->any())
                    <div class="alert alert-danger">
                        Foram encontrados alguns erros no preenchimento
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif

                    @if(session()->has('message'))
                    <div class="alert alert-success">
                        {{ session()->get('message') }}
                    </div>
                    @endif


                    <div class=" form-row">
                        <div class=" form-group col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <label class="col-form-label-lg" for="name">Nome completo do responsável</label>
                            <input required type="text" class=" form-control form-control-lg" name="name" value="{{ old('name') }}" id="name" placeholder="Digite seu nome completo">
                        </div>
                         <!-- <div class=" form-group col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                            <label class="col-form-label-lg" for="birth">Data de nascimento</label>
                            <input required type="text" class=" form-control form-control-lg" name="birth" value="{{ old('birth') }}" id="birth" data-mask="00/00/0000" placeholder="dd/mm/aaaa">
                        </div>
                      <div class=" form-group col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <label class="col-form-label-lg" for="responsible">Responsável</label>
                            <input required type="text" class=" form-control form-control-lg" name="responsible" value="{{ old('responsible') }}" id="responsible" placeholder="Digite o nome completo do responsável">
                        </div>-->
                        <div class=" form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                            <label class="col-form-label-lg" for="mobile">WhatsApp (Celular)</label>
                            <input required type="text" class=" form-control form-control-lg" name="mobile" value="{{ old('mobile') }}" id="mobile" data-mask="(00) 0 0000-0000" placeholder="(11) 9xxxx-xxxx">
                        </div>
                        <div class=" form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                            <label class="col-form-label-lg" for="phone">Telefone Fixo</label>
                            <input type="text" class=" form-control form-control-lg" name="phone" value="{{ old('phone') }}" id="phone" data-mask="(00) 0000-0000" placeholder="(11) xxxx-xxxx">
                        </div>
                        <div class=" form-group col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
                            <label class="col-form-label-lg" for="email">E-mail</label>
                            <input required type="email" class=" form-control form-control-lg" name="email" id="email" value="{{ old('email') }}" placeholder="seu@email.com">
                        </div>
                        <!--<div class=" form-group col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                            <label class="col-form-label-lg" for="zipcode">CEP</label>
                            <input required type="text" class=" form-control form-control-lg" name="zipcode" id="zipcode" value="{{ old('zipcode') }}" data-mask="00000-000" placeholder="00000-000">
                        </div>
                        <div class=" form-group col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                            <label class="col-form-label-lg" for="address">Endereço</label>
                            <input required type="text" class=" form-control form-control-lg" readonly="readonly" name="address" value="{{ old('address') }}" id="address" placeholder="Nome da rua, estrada, avenida...">
                        </div>
                        <div class=" form-group col-xl-3 col-lg-3 col-md-3 col-sm-4 col-4">
                            <label class="col-form-label-lg" for="number">Número</label>
                            <input required type="text" class=" form-control form-control-lg" name="number" id="number" value="{{ old('number') }}" placeholder="Número">
                        </div>
                        <div class=" form-group col-xl-3 col-lg-3 col-md-3 col-sm-8 col-8">
                            <label class="col-form-label-lg" for="address2">Complemento</label>
                            <input type="text" class=" form-control form-control-lg" name="address2" id="address2" value="{{ old('address2') }}" placeholder="Complemento">
                        </div>
                        <div class=" form-group col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                            <label class="col-form-label-lg" for="district">Bairro</label>
                            <input required type="text" class=" form-control form-control-lg" readonly="readonly" value="{{ old('district') }}" name="district" id="district" placeholder="Bairro">
                        </div>
                        <div class=" form-group col-xl-3 col-lg-3 col-md-3 col-sm-10 col-10">
                            <label class="col-form-label-lg" for="city">Cidade</label>
                            <input required type="text" class=" form-control form-control-lg" readonly="readonly" name="city" value="{{ old('city') }}" id="city" placeholder="Cidade">
                        </div>
                        <div class=" form-group col-xl-3 col-lg-3 col-md-3 col-sm-2 col-2">
                            <label class="col-form-label-lg" for="state">Estado</label>
                            <input required type="text" class=" form-control form-control-lg" readonly="readonly" name="state" value="{{ old('state') }}" id="state" placeholder="UF">
                        </div>
                        <div class=" form-group col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                            <label class="col-form-label-lg" for="school">Escola</label>
                            <input required list="school" name="school" value="{{ old( 'school')}}"" class=" form-control form-control-lg" placeholder="Digite o nome da escola">
                            <datalist id="school">
                                @foreach($schools as $school)
                                <option value="{{$school->name}}" > @endforeach
                            </datalist>
                            <small id="emailHelp" class="form-text text-muted">Nome da escola que estuda atualmente</small>
                        </div>-->
                        <div class=" form-group col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <label class="col-form-label-lg" for="level">Série</label>
                            <select class="form-control form-control-lg" name="level" id="level" required>
                                <option disabled selected hidden>Selecione</option>
                                <option {{ old( 'level')=="N1 da Educação Infantil" ? 'selected' : '' }} value="N1 da Educação Infantil">N1 da Educação Infantil </option>
                                <option {{ old( 'level')=="N2 da Educação Infantil" ? 'selected' : '' }} value="N2 da Educação Infantil">N2 da Educação Infantil </option>
                                <option {{ old( 'level')=="N3 da Educação Infantil" ? 'selected' : '' }} value="N3 da Educação Infantil">N3 da Educação Infantil </option>
                                <option {{ old( 'level')=="1ª ano do Ensino Fundamental" ? 'selected' : '' }} value="1ª ano do Ensino Fundamental">1ª ano do Ensino Fundamental</option>
                                <option {{ old( 'level')=="2ª ano do Ensino Fundamental" ? 'selected' : '' }} value="2ª ano do Ensino Fundamental">2ª ano do Ensino Fundamental</option>
                                <option {{ old( 'level')=="3ª ano do Ensino Fundamental" ? 'selected' : '' }} value="3ª ano do Ensino Fundamental">3º ano do Ensino Fundamental</option>
                                <option {{ old( 'level')=="4ª ano do Ensino Fundamental" ? 'selected' : '' }} value="4ª ano do Ensino Fundamental">4º ano do Ensino Fundamental</option>
                                <option {{ old( 'level')=="5ª ano do Ensino Fundamental" ? 'selected' : '' }} value="5ª ano do Ensino Fundamental">5º ano do Ensino Fundamental</option>
                                <option {{ old( 'level')=="6ª ano do Ensino Fundamental" ? 'selected' : '' }} value="6ª ano do Ensino Fundamental">6º ano do Ensino Fundamental</option>
                                <option {{ old( 'level')=="7ª ano do Ensino Fundamental" ? 'selected' : '' }} value="7ª ano do Ensino Fundamental">7º ano do Ensino Fundamental</option>
                                <option {{ old( 'level')=="8ª ano do Ensino Fundamental" ? 'selected' : '' }} value="8ª ano do Ensino Fundamental">8º ano do Ensino Fundamental</option>
                                <option {{ old( 'level')=="9ª ano do Ensino Fundamental" ? 'selected' : '' }} value="9ª ano do Ensino Fundamental">9º ano do Ensino Fundamental</option>
                                <option {{ old( 'level')=="1ª ano do Ensino Médio" ? 'selected' : '' }} value="1ª ano do Ensino Médio">1º ano do Ensino Médio</option>
                                <option {{ old( 'level')=="2ª ano do Ensino Médio" ? 'selected' : '' }} value="2ª ano do Ensino Médio">2º ano do Ensino Médio</option>
                                <option {{ old( 'level')=="3ª ano do Ensino Médio" ? 'selected' : '' }} value="3ª ano do Ensino Médio">3ª ano do Ensino Médio</option>
                            </select>
                            
                        </div><!--
                        <div class="form-group col-xl-2 col-lg-2 col-md-2 col-sm-12 col-12">
                            <label class="col-form-label-lg" for="period">Perídodo</label>
                            <select class="form-control form-control-lg" name="period" id="period">
                                <option value="" disabled selected hidden>Selecione</option>
                                <option value="Manhã" {{ old( 'period')=="Manhã" ? 'selected' : '' }}>Manhã</option>
                                <option value="Tarde" {{ old( 'period')=="Tarde" ? 'selected' : '' }}>Tarde</option>
                                <option value="Noite" {{ old( 'period')=="Noite" ? 'selected' : '' }}>Noite</option>
                                <option value="Integral" {{ old( 'period')=="Integral" ? 'selected' : '' }}>Integral</option>
                            </select>
                            <small id="emailHelp" class="form-text text-muted">Período atual do aluno</small>
                        </div>
                        <hr>
                        <div class=" form-group col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <label class="col-form-label-lg" for="time">Horário da prova</label>
                            <select class="form-control form-control-lg" name="time" id="time">
                                <option value="" disabled selected hidden>Selecione</option>
                                <option {{ old( 'time')=="09:00" ? 'selected' : '' }} value="09:00">01 de setembro às 09:00</option>
                                
                            </select>
                        </div>
                        <div class=" form-group col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <label class="col-form-label-lg" for="student">Você é aluno do Colégio Itaquá?</label>
                            <select class="form-control form-control-lg" name="student" id="student">
                                <option value="" disabled selected hidden>Selecione</option>
                                <option {{ old( 'student')=="Sim" ? 'selected' : '' }} value="Sim">Sim, estou estudando no Colégio Itaquá</option>
                                <option {{ old( 'student')=="Não" ? 'selected' : '' }} value="Não">Não, não estudo no Colégio Itaquá</option>
                            </select>
                        </div>-->
                        <div class=" form-group">
                            <div class="form-check">
                                <input required type="checkbox" class="form-check-input required" name="authorize" id="authorize" checked>
                                <label class="col-form" class=" form-check-label" for="authorize">Desejo receber as novidades e promoções do Colégio Itaquá</label>
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary btn-lg float-right">Enviar</button>
                </form>
            </div>
        </div>
    </div></main>

<footer class="footer">
<div class="col-12">
            <div class="row justify-content-center">
                <img src="./img/logo-colegio-itaqua.png" class="img-footer" />
            </div>
            <div class="text-footer">
                <h4>4647-0763 | 2829-4621<br/>
                  <small>Av. ítalo Adami, 1386,  Vila Zeferina
                  Itaquaquecetuba -SP</small>
               </h4>
            
            </div>
        </div>
</footer>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <script type="text/javascript" src="{{URL::asset('js/jquery.mask.js')}}"></script>
    <script type="text/javascript" src="{{URL::asset('js/mask.js')}}"></script>
    <script type="text/javascript" src="{{URL::asset('js/zipcode.js')}}"></script>


</body>
</html>