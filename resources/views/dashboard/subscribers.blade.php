@extends('layouts.app')

@section('content')

            <main class="main-content bgc-grey-100">
                <div id="mainContent">
                    <div class="container-fluid">
                        <h4 class="c-grey-900 mT-10 mB-30">{{$total}} cadastros</h4>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="bgc-white bd bdrs-3 p-20 mB-20">
                                    
                                    <table id="dataTable" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                            <th>ID</th>
                                                <th>Candidato</th>
                                                <th>Responsável</th>
                                                <th>Idade</th>
                                                <th>Telefone</th>
                                                <th>Celular</th>
                                                <th>E-mail</th>
                                                <th>Escola</th>
                                                <th>Série</th>
                                                <th>Período</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th>ID</th>
                                                <th>Candidato</th>
                                                <th>Responsável</th>
                                                <th>Idade</th>
                                                <th>Telefone</th>
                                                <th>Celular</th>
                                                <th>E-mail</th>
                                                <th>Escola</th>
                                                <th>Série</th>
                                                <th>Período</th>
                                            </tr>
                                        </tfoot>
                                        <tbody>
                                            @foreach($subscribers as $subscriber)
                                            <tr>
                                            <td>{{$subscriber->id}}</td>
                                            <td>{{$subscriber->first_name}}</td>
                                            <td>{{$subscriber->responsible}}</td>
                                            <td>{{\Carbon\Carbon::parse($subscriber->birth)->diff(\Carbon\Carbon::now())->format('%y')}}</td>
                                            <td>{{$subscriber->phone}}</td>
                                            <td>{{$subscriber->mobile}}</td>
                                            <td>{{$subscriber->email}}</td>
                                            <td>{{$subscriber->school}}</td>
                                            <td>{{$subscriber->level}}</td>
                                            <td>{{$subscriber->period}}</td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
@endsection