@extends('layouts.app')

@section('content')

            <main class="main-content bgc-grey-100">
                <div id="mainContent">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="bgc-white bd bdrs-3 p-20 mB-20">
                                    
                                    <table id="dataTable" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Candidato</th>
                                                <th>Escola</th>
                                                <th>Série</th>
                                                <th>Horário</th>
                                                <th>Português</th>
                                                <th>Matemática</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th>Candidato</th>
                                                <th>Escola</th>
                                                <th>Série</th>
                                                <th>Horário</th>
                                                <th>Português</th>
                                                <th>Matemática</th>
                                            </tr>
                                        </tfoot>
                                        <tbody>
                                            @foreach($subscribers as $subscriber)
                                            <tr>
                                                <td>{{$subscriber->name}}</td>
                                                <td>{{$subscriber->school}}</td>
                                                <td>{{$subscriber->level}}</td>
                                                <td>{{$subscriber->time}}</td>
                                                <td>{{$subscriber->portugues}}</td>
                                                <td>{{$subscriber->matematica}}</td>
                                                <td>Editar
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
@endsection