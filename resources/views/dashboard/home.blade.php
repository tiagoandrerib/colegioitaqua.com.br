@extends('layouts.app')

@section('content')
            <main class="main-content bgc-grey-100">
                <div id="mainContent">
                    <div class="row gap-20 masonry pos-r">
                        <div class="masonry-sizer col-md-6"></div>
                        <div class="masonry-item w-100">
                            <div class="row gap-20">
                                <div class="col-md-3">
                                    <div class="layers bd bgc-white p-20">
                                        <div class="layer w-100 mB-10">
                                            <h6 class="lh-1">Total Visits</h6></div>
                                        <div class="layer w-100">
                                            <div class="peers ai-sb fxw-nw">
                                                <div class="peer peer-greed"><span id="sparklinedash"></span></div>
                                                <div class="peer"><span class="d-ib lh-0 va-m fw-600 bdrs-10em pX-15 pY-15 bgc-green-50 c-green-500">{{number_format($sessions, 0, ',', '.')}}</span></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="layers bd bgc-white p-20">
                                        <div class="layer w-100 mB-10">
                                            <h6 class="lh-1">Total Page Views</h6></div>
                                        <div class="layer w-100">
                                            <div class="peers ai-sb fxw-nw">
                                                <div class="peer peer-greed"><span id="sparklinedash2"></span></div>
                                                <div class="peer"><span class="d-ib lh-0 va-m fw-600 bdrs-10em pX-15 pY-15 bgc-red-50 c-red-500">{{number_format($pageviews, 0, ',', '.')}}</span></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="layers bd bgc-white p-20">
                                        <div class="layer w-100 mB-10">
                                            <h6 class="lh-1">Unique Visitor</h6></div>
                                        <div class="layer w-100">
                                            <div class="peers ai-sb fxw-nw">
                                                <div class="peer peer-greed"><span id="sparklinedash3"></span></div>
                                                <div class="peer"><span class="d-ib lh-0 va-m fw-600 bdrs-10em pX-15 pY-15 bgc-purple-50 c-purple-500">{{number_format($newUsers, 0, ',', '.')}}</span></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="layers bd bgc-white p-20">
                                        <div class="layer w-100 mB-10">
                                            <h6 class="lh-1">Conversion</h6></div>
                                        <div class="layer w-100">
                                            <div class="peers ai-sb fxw-nw">
                                                <div class="peer peer-greed"><span id="sparklinedash4"></span></div>
                                                <div class="peer"><span class="d-ib lh-0 va-m fw-600 bdrs-10em pX-15 pY-15 bgc-blue-50 c-blue-500">{{ number_format($goalConversionRateAll, 2)}}%</span></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="masonry-item col-md-6">
                            <div class="bd bgc-white">
                                <div class="layers">
                                    <div class="layer w-100 p-20">
                                        <h6 class="lh-1">Reports</h6></div>
                                    <div class="layer w-100">
                                        <div class="bgc-light-blue-500 c-white p-20">
                                            <div class="peers ai-c jc-sb gap-40">
                                                <div class="peer peer-greed">
                                                    
                                                    <p class="mB-0">Cadastros por série</p>
                                                </div>
                                                <div class="peer">
</div>
                                            </div>
                                        </div>
                                        <div class="table-responsive p-20">
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                        <th class="bdwT-0">Total</th>
                                                        <th class="bdwT-0">Séie</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($groupsBy as $name => $subscriber)
                                                    <tr>
                                                      <td>{{$subscriber}}</td>
                                                      <td class="fw-600">{{$name}}</td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="ta-c bdT w-100 p-20"><a href="{{route('admin.subscribers')}}">Visualizar todos os cadastros</a></div>
                            </div>
                        </div>

                          <div class="masonry-item col-md-6">
                            <div class="bd bgc-white">
                                <div class="layers">
                                    <div class="layer w-100 p-20">
                                        <h6 class="lh-1">Reports</h6></div>
                                    <div class="layer w-100">
                                        <div class="bgc-light-blue-500 c-white p-20">
                                            <div class="peers ai-c jc-sb gap-40">
                                                <div class="peer peer-greed">
                                                    
                                                    <p class="mB-0">Cadastros por horário</p>
                                                </div>
                                                <div class="peer">
                                                    
                                              </div>
                                            </div>
                                        </div>
                                        <div class="table-responsive p-20">
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                        <th class="bdwT-0">Total</th>
                                                        <th class="bdwT-0">Horário</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($times as $name => $subscriber)
                                                    <tr>
                                                      <td>{{$subscriber}}</td>
                                                      <td class="fw-600">{{$name}}</td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="ta-c bdT w-100 p-20"><a href="{{route('admin.subscribers')}}">Visualizar todos os cadastros</a></div>
                            </div>
                        </div>
                    </div>
                </div>
            @endsection