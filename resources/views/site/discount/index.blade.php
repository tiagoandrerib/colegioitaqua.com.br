<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Desconto - Colégio Itaquá</title>
    <meta name="description" content="Garanta seu desconto para estudar no Colégio Itaquá.">
    <meta name="keywords" content="Colégio Itaqua, Itaquá, Itaquaquecetuba, Escola particular, colégio particular, ensino médio, ensino fundamental I, ensino fundamental II">

    <link href="https://fonts.googleapis.com/css?family=Crimson+Text:400,400i,600|Montserrat:200,300,400"
          rel="stylesheet">

    <link rel="stylesheet" href="{{ asset('landing/assets/css/bootstrap/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('landing/assets/fonts/ionicons/css/ionicons.min.css') }}">
    {{--<link rel="stylesheet" href="{{ asset('landing/assets/fonts/law-icons/font/flaticon.css') }}">--}}
    <link rel="stylesheet" href="{{ asset('landing/assets/fonts/fontawesome/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('landing/assets/css/slick.css') }}">
    <link rel="stylesheet" href="{{ asset('landing/assets/css/slick-theme.css') }}">
    <link rel="stylesheet" href="{{ asset('landing/assets/css/helpers.css') }}">
    <link rel="stylesheet" href="{{ asset('landing/assets/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('landing/assets/css/landing-2.css') }}">
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-5L3BQML');</script>
    <!-- End Google Tag Manager -->
</head>

<body data-spy="scroll" data-target="#pb-navbar" data-offset="200">
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5L3BQML"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<nav class="navbar navbar-expand-lg navbar-dark pb_navbar pb_scrolled-light" id="pb-navbar">
    <div class="container">
        <a class="navbar-brand" href="{{ route('discount.index') }}"><img src="https://www.colegioitaqua.com.br/img/colegio-itaqua-logo.png" width="150"></a>
        <button class="navbar-toggler ml-auto" type="button" data-toggle="collapse" data-target="#probootstrap-navbar"
                aria-controls="probootstrap-navbar" aria-expanded="false" aria-label="Toggle navigation">
            <span><i class="ion-navicon"></i></span>
        </button>
        <!-- <div class="collapse navbar-collapse" id="probootstrap-navbar">
            <ul class="navbar-nav ml-auto">
              <li class="nav-item"><a class="nav-link" href="#section-home">Home</a></li>
              <li class="nav-item"><a class="nav-link" href="#section-features">Features</a></li>
              <li class="nav-item"><a class="nav-link" href="#section-reviews">Reviews</a></li>
              <li class="nav-item"><a class="nav-link" href="#section-pricing">Pricing</a></li>
              <li class="nav-item"><a class="nav-link" href="#section-faq">FAQ</a></li>
              <li class="nav-item cta-btn ml-xl-2 ml-lg-2 ml-md-0 ml-sm-0 ml-0"><a class="nav-link"
                  href="https://uicookies.com/" target="_blank"><span class="pb_rounded-4 px-4">Get Started</span></a></li>
            </ul>
          </div>-->
    </div>
</nav>
<!-- END nav -->


<section class="pb_cover_v3 overflow-hidden cover-bg-indigo cover-bg-opacity text-left pb_gradient_v1 pb_slant-light"
         style="padding: 0;"
         id="section-home">
    <div class="container">
        <div class="row align-items-center justify-content-center">
            <div class="col-md-6">
                <h2 class="heading mb-3" style="font-size: 35pt;"><b>Voucher exclusivo para você!</b></h2>
                <h4 class="heading mb-0" style="font-size: 20pt; font-weight: bold;">Parceria Estuda Mais e Colégio Itaquá com Bolsa Parcial.</h4>
                <div class="sub-heading">
                    <p class="mb-4">Convênio exclusivo para seu filho estudar, vagas para maternal, infantil, ensino fundamental e médio.</p>

                    <p class="mb-4" style="font-size: 12px">Válido  para agendamentos entre os dias 14 à 21 de dezembro.</p>
                </div>
            </div>

            <div class="col-md-5 relative align-self-center">

                <form action="{{ route('discount.store') }}" method="POST" class="bg-white rounded pb_form_v1">
                    {{ csrf_field() }}
                    <h2 class="mb-4 mt-0 text-center">Agendar visita</h2>
                    <div class="form-group">
                        <input type="text" name="name" class="form-control pb_height-50 reverse {{ $errors->has('name') ? 'border border-danger': '' }}" placeholder="Nome completo" value="{{ old('name') }}" required>
                        @if($errors->has('name'))
                            <span class="text-danger">{{ $errors->first('name') }}</span>
                        @endif
                    </div>
                    <div class="form-group">
                        <input type="text" name="mobile" class="form-control pb_height-50 reverse {{ $errors->has('mobile') ? 'border border-danger': '' }}" data-mask="(00) 0 0000-0000"
                               placeholder="WhatsApp / Celular" value="{{ old('mobile') }}" required>

                        @if($errors->has('mobile'))
                             <span class="text-danger">{{ $errors->first('mobile') }}</span>
                        @endif
                    </div>

                    <div class="form-group">
                        <input type="text" name="phone" class="form-control pb_height-50 reverse {{ $errors->has('phone') ? 'border border-danger': '' }}" data-mask="(00) 0000-0000"
                               placeholder="Telefone" value="{{ old('phone') }}" >

                        @if($errors->has('phone'))
                            <span class="text-danger">{{ $errors->first('phone') }}</span>
                        @endif
                    </div>

                    <div class="form-group">
                        <div class="pb_select-wrap">
                            <select name="level" class="form-control pb_height-50 reverse {{ $errors->has('level') ? 'border border-danger': '' }}" required>
                                <option hidden value="">Selecione a série...</option>
                                <option {{ old( 'level')=="N1 da Educação Infantil" ? 'selected' : '' }} value="N1 da Educação Infantil">N1 da Educação Infantil </option>
                                <option {{ old( 'level')=="N2 da Educação Infantil" ? 'selected' : '' }} value="N2 da Educação Infantil">N2 da Educação Infantil </option>
                                <option {{ old( 'level')=="N3 da Educação Infantil" ? 'selected' : '' }} value="N3 da Educação Infantil">N3 da Educação Infantil </option>
                                <option {{ old( 'level')=="1ª ano do Ensino Fundamental" ? 'selected' : '' }} value="1ª ano do Ensino Fundamental">1ª ano do Ensino Fundamental</option>
                                <option {{ old( 'level')=="2ª ano do Ensino Fundamental" ? 'selected' : '' }} value="2ª ano do Ensino Fundamental">2ª ano do Ensino Fundamental</option>
                                <option {{ old( 'level')=="3ª ano do Ensino Fundamental" ? 'selected' : '' }} value="3ª ano do Ensino Fundamental">3º ano do Ensino Fundamental</option>
                                <option {{ old( 'level')=="4ª ano do Ensino Fundamental" ? 'selected' : '' }} value="4ª ano do Ensino Fundamental">4º ano do Ensino Fundamental</option>
                                <option {{ old( 'level')=="5ª ano do Ensino Fundamental" ? 'selected' : '' }} value="5ª ano do Ensino Fundamental">5º ano do Ensino Fundamental</option>
                                <option {{ old( 'level')=="6ª ano do Ensino Fundamental" ? 'selected' : '' }} value="6ª ano do Ensino Fundamental">6º ano do Ensino Fundamental</option>
                                <option {{ old( 'level')=="7ª ano do Ensino Fundamental" ? 'selected' : '' }} value="7ª ano do Ensino Fundamental">7º ano do Ensino Fundamental</option>
                                <option {{ old( 'level')=="8ª ano do Ensino Fundamental" ? 'selected' : '' }} value="8ª ano do Ensino Fundamental">8º ano do Ensino Fundamental</option>
                                <option {{ old( 'level')=="9ª ano do Ensino Fundamental" ? 'selected' : '' }} value="9ª ano do Ensino Fundamental">9º ano do Ensino Fundamental</option>
                                <option {{ old( 'level')=="1ª ano do Ensino Médio" ? 'selected' : '' }} value="1ª ano do Ensino Médio">1º ano do Ensino Médio</option>
                                <option {{ old( 'level')=="2ª ano do Ensino Médio" ? 'selected' : '' }} value="2ª ano do Ensino Médio">2º ano do Ensino Médio</option>
                                <option {{ old( 'level')=="3ª ano do Ensino Médio" ? 'selected' : '' }} value="3ª ano do Ensino Médio">3ª ano do Ensino Médio</option>
                                <option {{ old( 'level')=="Integral" ? 'selected' : '' }} value="Integral">Integral</option>
                            </select>
                            </select>
                            @if($errors->has('level'))
                                <span class="text-danger">{{ $errors->first('level') }}</span>
                            @endif
                        </div>

                    </div>
                    <div class="form-group">
                        <input type="submit" class="btn btn-primary btn-lg btn-block pb_btn-pill  btn-shadow-blue"
                               value="Agendar">
                    </div>
                </form>

            </div>
        </div>
    </div>
</section>

<footer class="pb_footer bg-light" role="contentinfo">
    <div class="container">
        <div class="row text-center">
            <div class="col">
                <ul class="list-inline">
                    <li class="list-inline-item"><a href="https://www.facebook.com/colegioitaquaoficial/" target="_blank" class="p-2"><i class="fa fa-facebook"></i></a></li>
                    <li class="list-inline-item"><a href="https://www.instagram.com/colegioitaqua/" target="_blank" class="p-2"><i class="fa fa-instagram"></i></a></li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col text-center">
                <p class="pb_font-14">&copy; 2019. All Rights Reserved.</p>

            </div>
        </div>
    </div>
</footer>

<!-- loader -->
<div id="pb_loader" class="show fullscreen">
    <svg class="circular" width="48px" height="48px">
        <circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/>
        <circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10"
                stroke="#1d82ff"/>
    </svg>
</div>


<script src="{{ asset('landing/assets/js/jquery.min.js') }}"></script>
<script src="{{ asset('landing/assets/js/popper.min.js') }}"></script>
<script src="{{ asset('landing/assets/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('landing/assets/js/slick.min.js') }}"></script>
<script src="{{ asset('landing/assets/js/jquery.mb.YTPlayer.min.js') }}"></script>
<script src="{{ asset('landing/assets/js/jquery.waypoints.min.js') }}"></script>
<script src="{{ asset('landing/assets/js/jquery.easing.1.3.js') }}"></script>
<script src="{{ asset('landing/assets/js/main.js') }}"></script>
<script type="text/javascript" src="https://www.liceubrasil.com.br/js/jquery.mask.js"></script>
<script type="text/javascript" src="https://www.liceubrasil.com.br/js/mask.js"></script>
</body>

</html>