<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Desconto - Colégio Itaquá</title>
    <meta name="description" content="Free Bootstrap 4 Template by uicookies.com">
    <meta name="keywords"
          content="Free website templates, Free bootstrap themes, Free template, Free bootstrap, Free website template">

    <link href="https://fonts.googleapis.com/css?family=Crimson+Text:400,400i,600,700|Montserrat:200,300,400,700"
          rel="stylesheet">

    <link rel="stylesheet" href="{{ asset('landing/assets/css/bootstrap/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('landing/assets/fonts/ionicons/css/ionicons.min.css') }}">
    <link rel="stylesheet" href="{{ asset('landing/assets/fonts/law-icons/font/flaticon.css') }}">
    <link rel="stylesheet" href="{{ asset('landing/assets/fonts/fontawesome/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('landing/assets/css/slick.css') }}">
    <link rel="stylesheet" href="{{ asset('landing/assets/css/slick-theme.css') }}">
    <link rel="stylesheet" href="{{ asset('landing/assets/css/helpers.css') }}">
    <link rel="stylesheet" href="{{ asset('landing/assets/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('landing/assets/css/landing-2.css') }}">

    <script src="{{ asset('landing/assets/js/jquery.min.js') }}"></script>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-5L3BQML');</script>
    <!-- End Google Tag Manager -->
</head>

<body onload="screenshot()" data-spy="scroll" data-target="#pb-navbar" data-offset="200">
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5L3BQML"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<nav class="navbar navbar-expand-lg navbar-dark pb_navbar pb_scrolled-light" id="pb-navbar">
    <div class="container">
        <a class="navbar-brand" href="{{ route('discount.index') }}"><img src="https://www.colegioitaqua.com.br/img/colegio-itaqua-logo.png" width="150"></a>
        <button class="navbar-toggler ml-auto" type="button" data-toggle="collapse" data-target="#probootstrap-navbar"
                aria-controls="probootstrap-navbar" aria-expanded="false" aria-label="Toggle navigation">
            <span><i class="ion-navicon"></i></span>
        </button>
        <!-- <div class="collapse navbar-collapse" id="probootstrap-navbar">
            <ul class="navbar-nav ml-auto">
              <li class="nav-item"><a class="nav-link" href="#section-home">Home</a></li>
              <li class="nav-item"><a class="nav-link" href="#section-features">Features</a></li>
              <li class="nav-item"><a class="nav-link" href="#section-reviews">Reviews</a></li>
              <li class="nav-item"><a class="nav-link" href="#section-pricing">Pricing</a></li>
              <li class="nav-item"><a class="nav-link" href="#section-faq">FAQ</a></li>
              <li class="nav-item cta-btn ml-xl-2 ml-lg-2 ml-md-0 ml-sm-0 ml-0"><a class="nav-link"
                  href="https://uicookies.com/" target="_blank"><span class="pb_rounded-4 px-4">Get Started</span></a></li>
            </ul>
          </div>-->
    </div>
</nav>
<!-- END nav -->


<section class="pb_cover_v3 overflow-hidden cover-bg-indigo cover-bg-opacity text-left pb_gradient_v1 pb_slant-light"
         style="padding: 0;"
         id="section-home">
    <div class="container">
        <div class="row align-items-center justify-content-center">
            <div class="col-md-6">
                <h2 class="heading mb-3" style="font-size: 35pt;"><b>Parabéns!</b></h2>
                <h4 class="heading mb-3" style="font-size: 20pt; font-weight: bold;">Sua solicitação de pré-agendamento foi realizada com sucesso!</h4>
                <div class="sub-heading">
                    <p class="mb-4">Em breve entraremos em contato para confirmar.</p>
                </div>
            </div>
            <div class="col-md-1">
            </div>
            <div class="col-md-5 relative align-self-center">

                <div  id="comprovante" class="comprovante bg-white rounded pb_form_v1">
                    {{ csrf_field() }}

                    <div class="text-center">
                        {!!  QrCode::size(150)->generate((string)$subscriber->uuid);!!}
                    </div>

                    <h4 class="mb-4 mt-0 text-center">Comprovante de agendamento</h4>

                    <div ><b>Nome:</b> <br>{{ $subscriber->name }}<br><br></div>
                    <div ><b>WhatsApp:</b> <br>{{ $subscriber->mobile }}<br><br></div>

                    <div ><b>Série:</b> <br> {{ $subscriber->level }}.<br><br></div>

                </div>



                <a href="{{ route('download', ['id' => $subscriber->id]) }}" class="btn btn-primary btn-lg btn-block pb_btn-pill  btn-shadow-blue mt-3" target="_blank">Salvar comprovante</a>

            </div>
        </div>
    </div>
</section>


<footer class="pb_footer bg-light" role="contentinfo">
    <div class="container">
        <div class="row text-center">
            <div class="col">
                <ul class="list-inline">
                    <li class="list-inline-item"><a href="https://www.facebook.com/colegioitaquaoficial/" target="_blank" class="p-2"><i class="fa fa-facebook"></i></a></li>
                    <li class="list-inline-item"><a href="https://www.instagram.com/liceubrasil/" target="_blank" class="p-2"><i class="fa fa-instagram"></i></a></li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col text-center">
                <p class="pb_font-14">&copy; 2019. All Rights Reserved.</p>
                </p>
            </div>
        </div>
    </div>
</footer>

<!-- loader -->
<div id="pb_loader" class="show fullscreen">
    <svg class="circular" width="48px" height="48px">
        <circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/>
        <circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10"
                stroke="#1d82ff"/>
    </svg>
</div>



<script src="{{ asset('landing/assets/js/popper.min.js') }}"></script>
<script src="{{ asset('landing/assets/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('landing/assets/js/slick.min.js') }}"></script>
<script src="{{ asset('landing/assets/js/jquery.mb.YTPlayer.min.js') }}"></script>
<script src="{{ asset('landing/assets/js/jquery.waypoints.min.js') }}"></script>
<script src="{{ asset('landing/assets/js/jquery.easing.1.3.js') }}"></script>
<script src="{{ asset('landing/assets/js/main.js') }}"></script>
<script type="text/javascript" src="https://html2canvas.hertzen.com/dist/html2canvas.js"></script>
<script>

    /*function screenshot() {
        html2canvas(document.querySelector('.comprovante'), {
                scale: 2
            }
        ).then(function (canvas) {
            var a = document.createElement('a');
            a.href = canvas.toDataURL("image/png");
            a.download = 'download.png';
            a.click();
        });
    }*/

    function screenshot() {
        html2canvas(document.querySelector('.comprovante'), {
                scale: 2
            }
        ).then(function (canvas) {
            var a = document.createElement('a');
            a.href = canvas.toDataURL("image/png");
            a.download = 'download.png';
            var dataURL = canvas.toDataURL();
            //a.click();
            $.ajax({
                url: 'https://www.colegioitaqua.com.br/desconto/{{ $subscriber->id }}/update',
                type: 'POST',
                data: {
                    image: dataURL
                },
                dataType: 'json',
                success: function(response) {
                    if(response.success) {
                        // Post the imgur url to your server.
                        $.post("https://www.colegioitaqua.com.br/desconto/{{ $subscriber->id }}/update", response.data.link);
                    }
                }
            });
        });
    }
</script>



</body>

</html>