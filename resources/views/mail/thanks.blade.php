<head>
        <title>Colégio Itaquá</title>
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0" name="viewport">
        <meta content="text/html; charset=UTF-8" http-equiv="Content-Type">
        <meta content="IE=edge" http-equiv="X-UA-Compatible"> </head>
      
      <body bgcolor="#f6f6f6" style="-webkit-font-smoothing: antialiased; background: #f6f6f6; margin: 0 auto; padding: 0; width: 100%"><span style="color:transparent;visibility:hidden;display:none;opacity:0;height:0;width:0;font-size:0;"></span><img src="http://links.greenchef.com/e/open?_k=033fa65d4f964830b7cf8d6ebf276aab&amp;_u=auriguev%40indiana.edu&amp;_n=73601&amp;_t=112069&amp;_m=775fe52acb924da8be70612d8ba7e728"
          style="border:0;width:1px;height:1px;border-width:0px!important;display:none!important;line-height:0!important;" width="1" height="1">
        <style type="text/css">
          .ReadMsgBody {
            width: 100%;
            background-color: #fff;
          }
          .ExternalClass {
            width: 100%;
            background-color: #fff;
          }
          body {
            width: 100%;
            background-color: #f6f6f6;
            margin: 0 auto !important;
            padding: 0 !important;
            -webkit-font-smoothing: antialiased;
          }
          @font-face {
            font-family: 'Lato', Arial, sans-serif !important;
            font-style: normal;
            font-weight: 400;
            src: local('Lato'),
              local('Lato'),
              url(http://fonts.gstatic.com/s/lato/v11/qIIYRU-oROkIk8vfvxw6QvesZW2xOQ-xsNqO47m55DA.woff)
                format('woff');
          }
          img {
            text-decoration: none;
            border: 0;
          }
          @-ms-viewport {
            width: device-width;
          }
          @media only screen and (max-width: 550px) {
            .wrapper {
              width: 100%;
              padding: 0 !important;
            }
          }
          @media only screen and (max-width: 480px) {
            td[class="templateColumnContainer"] {
              display: block !important;
            }
            .imgClass {
              width: 100% !important;
              height: auto;
              padding-left: 20px;
              padding-right: 0;
            }
            .wrapper {
              width: 320px;
              padding: 0 !important;
            }
            .container {
              width: 100%;
              padding: 0 !important;
            }
            .margin-on {
              margin-bottom: 30px;
            }
            h1 {
              font-family: 'Lato', Arial, sans-serif !important;
              font-size: 25px;
              line-height: 25px;
              font-style: normal;
              font-weight: 100;
              src: local('Lato'),
                local('Lato'),
                url(http://fonts.gstatic.com/s/lato/v11/Kom15zUm24dIPfIRiNogNuvvDin1pK8aKteLpeZ5c0A.woff)
                  format('woff');
            }
            .mobile {
              padding: 15px !important;
              text-align: center;
            }
          }

          .button {
    _background-color: #4CAF50; /* Green */
    border: none;
    color: #4CAF50;
    padding: 10px 25px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 16px;
    margin: 4px 2px;
    cursor: pointer;
}
        </style>
        <div style="background: #f4f4f4; border: 0px solid #f4f4f4; margin: 0; padding: 0">
          <center>
            <table bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" style="background: #ffffff; border-collapse: collapse !important; max-width: 650px; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%">
              <!--img-->
              <tbody>
      
                <tr>
                  <td align="center" bgcolor="#ffffff" style="background: #ffffff; font-family: Lato, Helevetica, Arial, sans-serif; font-size: 18px; line-height: 28px; padding-bottom: 20px; padding-top: 0px" valign="top">
                    <img alt="Colégio Itaquá" src="{{asset('mail/top-email.png')}}" style="border: 0; display: block; max-width: 100%; min-height: auto; text-decoration: none; vertical-align: top; width: 100%"></a>
                  </td>
                </tr>
                <tr>
                  <td align="center" bgcolor="#ffffff" style="background: #ffffff; font-family: Lato, Helevetica, Arial, sans-serif; font-size: 18px; line-height: 28px; padding-bottom: 20px; padding-top: 20px" valign="top">
                    <h2 align="center" style="color: #707070; font-weight: 400; margin: 0; text-align: center">Olá {{$first_name}}<?php echo ('!')?></h2> </td>
                </tr>
              </tbody>
            </table>
            <table bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" class="mobile" style="background: #ffffff; border-collapse: collapse !important; max-width: 650px; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%">
              <tbody>
                <tr>
                  <td align="center">
                    <table bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" class="mobile" style="background: #ffffff; border-collapse: collapse !important; max-width: 650px; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%">
                      <tbody>
                        <tr>
                          <td align="center" bgcolor="#ffffff" style="background: #ffffff; padding-bottom: 0px; padding-top: 0" valign="top">
                            
                          </td>
                        </tr>
                      </tbody>
                    </table>
                    <table bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" class="mobile" style="background: #ffffff; border-collapse: collapse !important; max-width: 600px; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 96%">
                      <tbody>
                        <tr>
                          <td align="center" bgcolor="#ffffff" style="background: #ffffff; padding-bottom: 0px; padding-top: 0" valign="top">
                            <table align="center" border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse !important; max-width: 600px; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%">
                              <tbody>
                                <tr>
                                  <td align="left" bgcolor="#ffffff" style="background: #ffffff; font-family: Lato, Helevetica, Arial, sans-serif; font-size: 18px; line-height: 28px; padding: 10px 0px 20px" valign="top">
                                    <h2 align="center" style="color: #707070; font-size: 22px; font-weight: 400; line-height: 32px; margin: 0; padding-bottom: 20px; text-align: center">Recebemos com sucesso sua inscrição e o número do protocolo é <b style="color: #7da33a; text-decoration: none">{{$id or "201851888"}}</b><br>
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                    <table bgcolor="#ffffff" cellpadding="0" cellspacing="0" class="mobile" style="background: #ffffff; border-collapse: collapse !important; border: 1px solid #d1d2d1; max-width: 600px; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 96%">
                      <tbody>
                        <tr>
                          <td align="center" bgcolor="#ffffff" style="background: #ffffff; padding-bottom: 0px; padding-top: 0" valign="top">
                            <table align="center" border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse !important; max-width: 600px; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 80%">
                              <tbody>
                                <tr>
                                  <td align="left" bgcolor="#ffffff" style="background: #ffffff; font-family: Lato, Helevetica, Arial, sans-serif; font-size: 18px; line-height: 28px; padding: 10px 0px 20px" valign="top">
                                    <h2 align="center" style="color: #707070; font-size: 22px; font-weight: 400; line-height: 36px; margin: 0; padding-bottom: 20px; padding-top: 10px; text-align: center">Informações</h2>
                                    <table align="center" border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse !important; max-width: 600px; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%">
                                      <tbody>
                                        <tr>
                                          <td align="left" bgcolor="#ffffff" style="background: #ffffff; font-family: Lato, Helevetica, Arial, sans-serif; font-size: 18px; max-width: 30%; padding: 0px; width: 30%" valign="top">
                                            <p style="color: #707070; font-size: 14px; font-weight: 400; line-height: 22px; padding-bottom: 0px; text-align: left"><strong>Nome do aluno</strong><br>{{$name}}</p>
                                          </td>
                                        </tr>
                                        <!---->
                                      </tbody>
                                    </table>
                                    <hr>
                                    <table align="center" border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse !important; max-width: 600px; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%">
                                      <tbody>
                                        <tr>
                                          <td align="left" bgcolor="#ffffff" style="background: #ffffff; font-family: Lato, Helevetica, Arial, sans-serif; font-size: 18px; max-width: 80%; padding: 10x 0px 0px; width: 80%" valign="top">
                                            <p align="left" style="color: #707070; font-size: 14px; font-weight: 400; line-height: 22px; padding-bottom: 0px; text-align: left"><strong>Série atual</strong><br> {{$level or "3º ano ensino médio"}}</p>
                                          </td>
                                        </tr>
                                      </tbody>
                                    </table>

                                    <hr>
                                    <table align="center" border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse !important; max-width: 600px; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%">
                                      <tbody>
                                        <tr>
                                          <td align="left" bgcolor="#ffffff" style="background: #ffffff; font-family: Lato, Helevetica, Arial, sans-serif; font-size: 18px; max-width: 80%; padding: 10x 0px 0px; width: 80%" valign="top">
                                            <p align="left" style="color: #707070; font-size: 14px; font-weight: 400; line-height: 22px; padding-bottom: 0px; text-align: left"><strong>Contatos</strong><br> {{$mobile or "ERROR"}} | {{$phone or 'Não informado'}}<br> {{$email or "Não informado"}}</p>
                                          </td>
                                        </tr>
                                      </tbody>
                                    </table>
                                  </td>
                                </tr>
                              </tbody>
                            </table>

                          </td>
                        </tr>
                      </tbody>
                    </table>
                    <table bgcolor="#ffffff" cellpadding="0" cellspacing="0" class="mobile" style="background: #ffffff; border-collapse: collapse !important; max-width: 600px; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 96%">
                      <tbody>
                        <tr>
                          <td align="center" bgcolor="#ffffff" style="background: #ffffff; padding-bottom: 0px; padding-top: 30px" valign="top">
                            <table align="center" border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse !important; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 90%">
                              <tbody>
                                <tr>
                                  <td align="left" bgcolor="#ffffff" style="background: #ffffff; font-family: Lato, Helevetica, Arial, sans-serif; font-size: 18px; line-height: 28px; padding: 10px 0px 20px" valign="top"><img src="http://static.iterable.com/033fa65d4f964830b7cf8d6ebf276aab/16-11-08-03 location.png" style="border: 0px; padding-top: 10px; text-decoration: none; width: 25px"></td>
                                  <td align="left" bgcolor="#ffffff" style="background: #ffffff; font-family: Lato, Helevetica, Arial, sans-serif; font-size: 18px; line-height: 28px; max-width: 200px; padding: 10px 0px 20px"
                                    valign="top">
                                    <p align="left" style="color: #707070; font-size: 14px; font-weight: 400; line-height: 22px; padding-bottom: 0px; text-align: left"><strong>Local</strong><br> Av. Italo Adami, 1386<br> Vila Zeferina,
                                      <!-- --> Itaquaquecetuba
                                      <!-- --><br> Próximo ao mercado D'avo</p>
                                  </td>
                                  <td align="left" bgcolor="#ffffff" style="background: #ffffff; font-family: Lato, Helevetica, Arial, sans-serif; font-size: 18px; line-height: 28px; padding: 10px 0px 20px" valign="top"><img src="http://static.iterable.com/033fa65d4f964830b7cf8d6ebf276aab/16-11-08-04 calendar.png" style="border: 0px; padding-top: 10px; text-decoration: none; width: 28px"><br>  <img src="{{asset('img/clock.png')}}"
                                      style="border: 0px; padding-top: 15px; text-decoration: none; width: 28px"></td>
                                  <td align="left" bgcolor="#ffffff" style="background: #ffffff; font-family: Lato, Helevetica, Arial, sans-serif; font-size: 18px; line-height: 28px; padding: 10px 0px 20px"
                                    valign="top">
                                    <p align="left" style="color: #707070; font-size: 14px; font-weight: 400; line-height: 22px; padding-bottom: 0px; text-align: left"><strong>Data</strong><br> Sábado, 01/09/2018 <br>  
                                    
                                    <strong>Horário</strong>
                                    <br>
                                    @if (empty($time))
                                     ERROR: <a href="{{route('cadastro', $id)}}" style="color: #7DA33A; text-decoration: none" target="_blank">Corrigir</a></p>
                                     @else
                                     {{$time}}</p>
                                     @endif
                                  </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                                                  
                        </tr>
                      </tbody>
                    </table>

                   

                    <table bgcolor="#ffffff" cellpadding="0" cellspacing="0" class="mobile" style="background: #ffffff; border-collapse: collapse !important; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%">
                      <tbody>
                        <tr>
                          <td align="center" bgcolor="#ffffff" style="background: #ffffff; padding-bottom: 0px; padding-top: 0" valign="top">
                            <table align="center" border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse !important; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 80%">
                              <tbody>

                              

                                <tr>
                                  <td align="left" bgcolor="#ffffff" style="background: #ffffff; font-family: Lato, Helevetica, Arial, sans-serif; font-size: 18px; line-height: 28px; padding: 10px 0px 20px" valign="top">
                                    <p align="left" style="color: #707070; font-size: 14px; font-weight: 400; line-height: 22px; padding-bottom: 0px; text-align: left"><strong>Alterar suas informações</strong><br>Clique <a href="{{route('cadastro', $id)}}" style="color: #7DA33A; text-decoration: none" target="_blank">aqui</a> para alterar ou corrigir suas informações. <br><br> <strong>O que precisa levar</strong><br>Documento original com foto<br> Caneta preta ou azul, lápis e borrachade<br> <br><strong>O que é proibido</strong>&nbsp;<br>
                                        Aparelho eletrônico, como celular, tablet, relógio <br> Calculadora, fone de ouvido, óculos escuros<br>Livros e anotações.<br> <br> <strong>Ficou com alguma dúvida?</strong><br> Ligue 11 4647-0763 ou 2829-4621<br>                                &nbsp;</p>
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </td>
                </tr>
              </tbody>
            </table>
            <table align="center" bgcolor="#f4f4f4" border="0" cellpadding="0" cellspacing="0" style="background: #f4f4f4; border-collapse: collapse !important; border: 0 #f4f4f4; max-width: 650px; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%">
              <tbody>
                <!---footer-->
                <tr>
                  <td align="left" bgcolor="#f4f4f4" class="mobile footerBorder" style="background: #f4f4f4; padding: 0px 5px" valign="top">
                    <table align="center" border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse !important; mso-table-lspace: 0pt; mso-table-rspace: 0pt">
                      <tbody>
                        <tr>
                          <td align="center" style="padding-bottom: 20px" valign="top">
                            <table align="center" border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse !important; mso-table-lspace: 0pt; mso-table-rspace: 0pt">
                              <tbody>
                                <tr>
                                  <td align="center" style="padding-bottom: 0px; padding-top: 30px" valign="top">
                                    <a href="https://www.facebook.com/colegioitaquaoficial/"
                                      style="border: 0px; color: #7DA33A; text-decoration: none" target="_blank"><img src="http://static.iterable.com/033fa65d4f964830b7cf8d6ebf276aab/16-11-16-footer_Icons_06.jpg" style="border: 0px; text-decoration: none" width="45"></a>
                                    <a href="https://twitter.com/colegioitaqua"
                                      style="border: 0px; color: #7DA33A; text-decoration: none" target="_blank"><img src="http://static.iterable.com/033fa65d4f964830b7cf8d6ebf276aab/16-11-16-footer_Icons_09.jpg" style="border: 0px; text-decoration: none" width="45"></a>
                                    <a href="https://www.instagram.com/colegioitaqua/"
                                      style="border: 0px; color: #7DA33A; text-decoration: none" target="_blank"><img src="http://static.iterable.com/033fa65d4f964830b7cf8d6ebf276aab/16-11-16-footer_Icons_07.jpg" style="border: 0px; text-decoration: none" width="45"></a>
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </td>
                </tr>
              </tbody>
            </table>
            <table bgcolor="#f4f4f4" border="0" cellpadding="0" cellspacing="0" style="background: #f4f4f4; border-collapse: collapse !important; max-width: 650px; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%">
              <tbody>
                <tr>
                  <td align="center" bgcolor="#f4f4f4" style="background: #f4f4f4; padding-bottom: 40px; padding-top: 0px" valign="top">
                    <table align="center" border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse !important; max-width: 550; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%">
                      <tbody>
                        <tr>
                          <td align="center" style="padding-left: 20px; padding-right: 20px" valign="top">
                            <table align="center" border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse !important; max-width: 650px; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%">
                              <tbody>
                                <tr>
                                  <td align="center" style="max-width: 650px; width: 100%" valign="top">
                                    <p align="center" style="color: #b7b7b7; font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; font-size: 13px; font-weight: normal; line-height: 20px; margin: 0; padding: 0; text-align: center"><a href="http://colegioitaqua.com.br/regulamento-concurso-bolsa-2019.pdf" style="color: #b7b7b7; text-decoration: none" target="_blank">Consulte o regulamento</a> 
                                                                </p>
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </td>
                </tr>
                <!---footer-->
              </tbody>
            </table>
          </center>
        </div>
      </body>