<!DOCTYPE html>
    <html lang="pt-BR">
        <head>
            <title>Concurso de Bolsa Colégio Itaquá 2018</title>
            <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
            <link rel="stylesheet" href="{{ URL::asset('css/main.css') }}">
            <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
            {!! AnalyticsTracking::render() !!}

        </head>
        <body>

                <header class="container-fluid">
                        <div class="row justify-content-center">
                            <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                <img src="{{URL::asset('/img/logo.png')}}" class="img-fluid" />
                            </div>
                        </div>
                </header>

            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-xl-10 col-lg-10 col-md-12 col-sm-12 col-12">

                            <div class="container">
                                    <div class="primary">
                                       <div class="row justify-content-sm-center">
                                          <div class="col-xl-8 col-lg-8 col-md-12 col-sm-12 col-12">
                                          <div class="help message">
                                                   <h3>Olá <b>{{$name or 'NULL'}}</b>!</h3>
                                                   <p>Sua inscrição número <b>{{$id or 'NULL'}}</b> foi confirmada com sucesso. 
                                                    <br/>Em breve você receberá em seu e-mail todas as informações da prova.
                                                    <br/><br/><b>Prova: </b>01 de setembro de 2018
                                                    <br><b>Horário:</b> {{$time or 'NULL'}}
                                                    <br><b>Taxa: </b>1 litro de leite integral 
                                                    <br/><br/>Boa sorte!
                                                 </p>
                                               </div>
                                             <!--<div class="video">
                                                <div class="embed-responsive embed-responsive-16by9">
                                                   <iframe class="embed-responsive-item" width="560" height="315" src="https://www.youtube.com/embed/-kotQnY-IJ8?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
                                                </div>
                                                
                                             </div>
                                          </div>
                                       </div>
                                       <div class="row justify-content-center">
                                          <div class="col-xl-5 col-lg-6 col-md-7 col-sm-9 col-12">
                                             <div class="button">
                                               <a href="whatsapp://send?text=*SORTEIO DE VÁRIAS CAMISETAS DA MARCHA PARA JESUS* %0A %0AConcorra diariamente a uma camiseta da Marcha para Jesus Itaquá 2018 %0A%0A *INSCREVA-SE* %0A%0Ahttp://www.marchaitaqua.com.br %0A%0A *COMPARTILHE E AUMENTE SUA CHANCE*" data-action="share/whatsapp/share">
                                                   <div class="button-main">COMPARTILHAR NO WHATSAPP</div>
                                                </a>
                                             </div>-->
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                           
                        
                    </div>
                </div>
            </div>

            <footer class="container-fluid">
                    <div class="col-12">
                        <div class="row justify-content-center">
                            
                        <img src="{{URL::asset('/img/logo-colegio-itaqua.png')}}" class="img-footer" />
                        </div>
                        <div class="text-footer">
                    <h4>4647-0763 | 2829-4628<br/>
                        <small>Av. ítalo Adami, 1422,  Vila Zeferina
                                Itaquaquecetuba -SP</small></h4></div>
                               
                            </div>
                    
                </footer>

            <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
            <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
            
            <script type="text/javascript" src="{{URL::asset('js/jquery.mask.js')}}"></script>
            <script type="text/javascript" src="{{URL::asset('js/mask.js')}}"></script>
            <script type="text/javascript" src="{{URL::asset('js/zipcode.js')}}"></script>
            

        </body>
    </html>